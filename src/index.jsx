import 'bootstrap/dist/css/bootstrap.min.css'
import ReactDOM from 'react-dom/client'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import Exception from './components/Exception'
import './index.css'
import Footer from './pages/Footer'
import Header from './pages/Header'
import Home from './pages/Home'
import Albums from './pages/jsonfakeholder/Albums'
import Comments from './pages/jsonfakeholder/Comments'
import Posts from './pages/jsonfakeholder/Posts'
import Todos from './pages/jsonfakeholder/Todos'
import Users from './pages/jsonfakeholder/Users'
import UseMemoSample from './pages/samples/useMemo'
import './styles/pages.css'
// import reportWebVitals from './reportWebVitals';

const root = ReactDOM.createRoot(document.getElementById('root'))

root.render(
  <BrowserRouter>
    <Header />
    <Routes>
      <Route exact path="/" element={<Home />} />
      <Route path="/jsonfakeholder" element={<Users />} />
      <Route path="/jsonfakeholder/users/:userId" element={<Users />} />
      <Route path="/jsonfakeholder/users" element={<Users />} />
      <Route path="/jsonfakeholder/todos/:userId" element={<Todos />} />
      <Route path="/jsonfakeholder/todos" element={<Todos />} />
      <Route path="/jsonfakeholder/posts/:userId" element={<Posts />} />
      <Route path="/jsonfakeholder/posts" element={<Posts />} />
      <Route path="/jsonfakeholder/comments/:postId" element={<Comments />} />
      <Route path="/jsonfakeholder/comments" element={<Comments />} />
      <Route path="/jsonfakeholder/albums" element={<Albums />} />
      <Route path="/jsonfakeholder/albums/:userId" element={<Albums />} />
      <Route path="/samples/useMemo" element={<UseMemoSample />} />
      <Route component={Exception} />
    </Routes>
    <Footer />
  </BrowserRouter>,
)
// reportWebVitals();
