import { useMemo, useState } from 'react'

export function memUseState() {}

export function useMemoWithMap(input, size = 10) {
  const [duration, setDuration] = useState(0)
  const [datasMap, setDatasMap] = useState(new Map())

  return useMemo(() => {
    return calculate(input)
  }, [input])

  function addData(key, value) {
    console.log('addData map')
    setDatasMap((prevMap) => new Map(prevMap.set(key, value)))
  }

  function updateDuration(startDate) {
    var endDate = new Date().getTime()
    var dur = endDate - startDate
    setDuration(dur)
  }

  function calculate(input) {
    console.log('--calculate map: size=' + datasMap.size)

    let count = parseInt(input)
    if (!Number.isInteger(count)) {
      console.log('not an integer')
      return { result: 0, duration: '--', size: datasMap.size }
    }

    var startDate = new Date().getTime()
    const cachedValue = datasMap.get(input)

    if (cachedValue) {
      console.log('calculate map found')
      updateDuration(startDate)
      return { result: cachedValue, duration: 'XX', size: datasMap.size }
    }

    console.log('!found')

    var resultD = 0

    while (count > 1) {
      resultD += count
      count--
    }

    addData(input, resultD)
    updateDuration(startDate)

    //console.log('resultData=' + resultD + ';duration=' + duration)

    return { result: resultD, duration: duration, size: datasMap.size }
  }
}

export function useMemoWithArray(input, size = 10) {
  const [duration, setDuration] = useState(0)
  const [datas, setDatas] = useState([])

  return useMemo(() => {
    return calculate(input)
  }, [input])

  function addData(key, value) {
    console.log('addData array')
    setDatas([...datas, { input: key, output: value }])
  }

  function updateDuration(startDate) {
    var endDate = new Date().getTime()
    var dur = endDate - startDate
    setDuration(dur)
  }

  function calculate(input) {
    console.log('--calculate array: size=' + datas.length)

    let count = parseInt(input)
    if (!Number.isInteger(count)) {
      console.log('not an integer')
      return { result: 0, duration: '--', size: datas.length }
    }

    var startDate = new Date().getTime()
    //    if (datas.size >= 1) {
    let cachedValue = datas.find((element) => element.input == input)

    if (cachedValue) {
      console.log('calculate array found')
      updateDuration(startDate)
      return { result: cachedValue.output, duration: 'XX', size: datas.length }
    }
    //  }

    console.log('!found')
    var resultD = 0

    while (count > 1) {
      resultD += count
      count--
    }

    addData(input, resultD)
    updateDuration(startDate)

    //console.log('resultArray=' + resultD + ';duration=' + duration)

    return { result: resultD, duration: duration, size: datas.length }
  }
}

export function clearItemToMemoDatasMap() {}

/*
export function memoUseState() {
  const [waitDuration, setWaitDuration] = useState(0)
  const [message, setMessage] = useState('')
  const [duration, setDuration] = useState(0)
  const [memoDatasMap, setMemoDatasMap] = useState(new Map())
}

export function clearItemToMemoDatasMap() {
  setMemoDatasMap(new Map())
}

export function addItemToMemoDatasMap(key, value) {
  setMemoDatasMap((prevMap) => new Map(prevMap.set(key, value)))
}

export function updateDuration(startDate) {
  var endDate = new Date().getTime()
  var dur = endDate - startDate
  setDuration(dur)
  var msg = 'duration=' + dur + ' for waitDuration=' + waitDuration
  setMessage(msg)
}

export function calculateWithMap(inputData) {
  console.log('calculateWithMap')

  var startDate = new Date().getTime()
  const found = memoDatasMap.get(inputData)

  if (found) {
    console.log('found')
    updateDuration(startDate)
    return found
  }

  console.log('!found')
  var count = parseFloat(inputData)
  var resultData = 0
  while (count > 1) {
    resultData += count
    count--
  }

  addItemToMemoDatasMap(inputData, resultData)
  updateDuration(startDate)

  return resultData
}
*/
