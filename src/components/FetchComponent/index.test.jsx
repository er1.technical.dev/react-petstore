import { assert, describe, it } from 'vitest'

const usersTest = [
  {
    id: 1,
    name: 'Leanne Graham',
    username: 'Bret',
    email: 'Sincere@april.biz',
    address: {
      street: 'Kulas Light',
      suite: 'Apt. 556',
      city: 'Gwenborough',
      zipcode: '92998-3874',
      geo: {
        lat: '-37.3159',
        lng: '81.1496',
      },
    },
    phone: '1-770-736-8031 x56442',
    website: 'hildegard.org',
    company: {
      name: 'Romaguera-Crona',
      catchPhrase: 'Multi-layered client-server neural-net',
      bs: 'harness real-time e-markets',
    },
  },
  {
    id: 2,
    name: 'Ervin Howell',
    username: 'Antonette',
    email: 'Shanna@melissa.tv',
    address: {
      street: 'Victor Plains',
      suite: 'Suite 879',
      city: 'Wisokyburgh',
      zipcode: '90566-7771',
      geo: {
        lat: '-43.9509',
        lng: '-34.4618',
      },
    },
    phone: '010-692-6593 x09125',
    website: 'anastasia.net',
    company: {
      name: 'Deckow-Crist',
      catchPhrase: 'Proactive didactic contingency',
      bs: 'synergize scalable supply-chains',
    },
  },
]

export function provideUsersTest() {
  return usersTest
}

// @TODO resolve
//global.fetch = jest.fn(() =>
/*

jest.fn(() =>
  Promise.resolve({
    json: () => Promise.resolve(usersTest),
  }),
)

beforeEach(() => {
  fetch.mockClear()
})
*/

describe('FetchCompoenets', () => {
  it('foo', () => {
    assert.equal('start', 'start')
  })
  /*,
    it('should return a default search term and original items', () => {
      const { data, isLoading, error } = renderHook(() => useFetch(import.meta.env.VITE_APP_URL_COMMENTS))

      //      expect(data).toBe(usersTest)
      //expect(isLoading).toEqual(true)
      //expect(error).toBe(null)
    })
    */
  //it('users list', async () => {
  /*
  const { data, isLoading, error } = useFetch(
    process.env.REACT_APP_URL_USERS
)

  expect(data).toEqual(usersTest);
  expect(isLoading).toEqual(false);
  expect(error).toEqual(false);
  expect(fetch).toHaveBeenCalledTimes(1);
    */
})
