import { render } from '@testing-library/react'
import { describe } from 'vitest'
import Comments from '.'

const commentsTests = [
  {
    postId: 1,
    id: 1,
    name: 'id labore ex et quam laborum',
    email: 'Eliseo@gardner.biz',
    body: 'laudantium enim quasi est quidem magnam voluptate ipsam eos\ntempora quo necessitatibus\ndolor quam autem quasi\nreiciendis et nam sapiente accusantium',
  },
  {
    postId: 1,
    id: 2,
    name: 'quo vero reiciendis velit similique earum',
    email: 'Jayne_Kuhic@sydney.com',
    body: 'est natus enim nihil est dolore omnis voluptatem numquam\net omnis occaecati quod ullam at\nvoluptatem error expedita pariatur\nnihil sint nostrum voluptatem reiciendis et',
  },
  {
    postId: 1,
    id: 3,
    name: 'odio adipisci rerum aut animi',
    email: 'Nikita@garfield.biz',
    body: 'quia molestiae reprehenderit quasi aspernatur\naut expedita occaecati aliquam eveniet laudantium\nomnis quibusdam delectus saepe quia accusamus maiores nam est\ncum et ducimus et vero voluptates excepturi deleniti ratione',
  },
]

export function provideCommentsTest() {
  return commentsTests
}

describe('Comments tests', () => {
  test('Comments', () => {
    render(<Comments />)
  })

  //test('comments list', async () => {
  /*
  jest.spyOn(global, 'fetch')
  .mockImplementation(() => Promise.resolve({
    status: 200,
    json: () => Promise.resolve({
      value: usersTest
    })
  }));
  */
  /*
    global.fetch = jest.fn(() =>
    Promise.resolve({
      json: () => Promise.resolve(usersTest),
    })
  );
*/
  /*
  act(() => {
    jest.advanceTimersByTime(1000);
  })
*/
  // jest.spyOn(global, "fetch").mockImplementation(provideUsersTest());
  // render(<Comments />);
  //console.log("data=" + data);
  // console.log("asFragment=" + asFragment);
  // const listUsers = await waitForElement(() => getByTestId('list-users'));
  //expect(getAllByRole('key').length).toBe(2)
  //screen.getByTestId("list-users");
})
