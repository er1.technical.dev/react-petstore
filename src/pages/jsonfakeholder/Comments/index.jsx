import { Spinner, Table } from 'react-bootstrap'
import { Link, useParams } from 'react-router-dom'
import { useFetch } from '../../../components/FetchComponent'

function Comments() {
  const { postId } = useParams()

  const { data, isLoading, error } = useFetch(import.meta.env.VITE_APP_URL_COMMENTS)

  if (error) {
    return <span>Il y a un problème</span>
  }

  return (
    <div className="container mt-4">
      {isLoading ? (
        <Spinner animation="grow" variant="danger">
          <span className="visually-hidden">Loading...</span>
        </Spinner>
      ) : (
        <span>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>id</th>
                <th>postId</th>
                <th>name</th>
                <th>email</th>
                <th>body</th>
              </tr>
            </thead>
            <tbody>
              {data
                ?.filter((item) => item.postId == postId || postId == null)
                .map((item) => (
                  <tr key={item.id}>
                    <td>{item.id}</td>
                    <td>
                      <Link to={`/jsonfakeholder/posts/${item.postId}`}>{item.postId}</Link>
                    </td>
                    <td>{item.name}</td>
                    <td>{item.email}</td>
                    <td>{item.body}</td>
                  </tr>
                ))}
            </tbody>
          </Table>
        </span>
      )}
    </div>
  )
}

export default Comments
