import { render } from '@testing-library/react'
import { describe } from 'vitest'
import Posts from '.'

const postTests = [
  {
    userId: 1,
    id: 1,
    title: 'sunt aut facere repellat provident occaecati excepturi optio reprehenderit',
    body: 'quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto',
  },
  {
    userId: 1,
    id: 2,
    title: 'qui est esse',
    body: 'est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla',
  },
  {
    userId: 1,
    id: 3,
    title: 'ea molestias quasi exercitationem repellat qui ipsa sit aut',
    body: 'et iusto sed quo iure\nvoluptatem occaecati omnis eligendi aut ad\nvoluptatem doloribus vel accusantium quis pariatur\nmolestiae porro eius odio et labore et velit aut',
  },
]

export function providePostsTest() {
  return postTests
}

describe('Posts tests', () => {
  test('Posts', () => {
    render(<Posts />)
  })

  //test('posts list', async () => {
  /*
  jest.spyOn(global, 'fetch')
  .mockImplementation(() => Promise.resolve({
    status: 200,
    json: () => Promise.resolve({
      value: usersTest
    })
  }));
  */
  /*
    global.fetch = jest.fn(() =>
    Promise.resolve({
      json: () => Promise.resolve(usersTest),
    })
  );
*/
  /*
  act(() => {
    jest.advanceTimersByTime(1000);
  })
*/
  // jest.spyOn(global, "fetch").mockImplementation(provideUsersTest());
  // render(<Posts />);
  //console.log("data=" + data);
  // console.log("asFragment=" + asFragment);
  // const listUsers = await waitForElement(() => getByTestId('list-users'));
  //expect(getAllByRole('key').length).toBe(2)
  //screen.getByTestId("list-users");
})
