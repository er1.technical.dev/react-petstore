import { Table } from 'react-bootstrap'
import { Link, useParams } from 'react-router-dom'
import { useFetch } from '../../../components/FetchComponent'

function Posts() {
  const { userId } = useParams()

  const { data, isLoading, error } = useFetch(import.meta.env.VITE_APP_URL_POSTS)

  if (error) {
    return <span>Il y a un problème</span>
  }

  return (
    <div className="container mt-4">
      {isLoading ? (
        <div>Loading</div>
      ) : (
        <span>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>id</th>
                <th>userId</th>
                <th>title</th>
                <th>body</th>
              </tr>
            </thead>
            <tbody>
              {data
                ?.filter((item) => item.userId == userId || userId == null)
                .map((item) => (
                  <tr key={item.id}>
                    <td>{item.id}</td>
                    <td>
                      <Link to={`/jsonfakeholder/users/${item.userId}`}>{item.userId}</Link>
                    </td>
                    <td>{item.title}</td>
                    <td>{item.body}</td>
                    <td>
                      <Link to={`/jsonfakeholder/comments/${item.id}`}>comments</Link>
                    </td>
                  </tr>
                ))}
            </tbody>
          </Table>
        </span>
      )}
    </div>
  )
}

export default Posts
