import { render } from '@testing-library/react'
import { describe, test } from 'vitest'
import Users from '.'

const usersTest = [
  {
    id: 1,
    name: 'Leanne Graham',
    username: 'Bret',
    email: 'Sincere@april.biz',
    address: {
      street: 'Kulas Light',
      suite: 'Apt. 556',
      city: 'Gwenborough',
      zipcode: '92998-3874',
      geo: {
        lat: '-37.3159',
        lng: '81.1496',
      },
    },
    phone: '1-770-736-8031 x56442',
    website: 'hildegard.org',
    company: {
      name: 'Romaguera-Crona',
      catchPhrase: 'Multi-layered client-server neural-net',
      bs: 'harness real-time e-markets',
    },
  },
  {
    id: 2,
    name: 'Ervin Howell',
    username: 'Antonette',
    email: 'Shanna@melissa.tv',
    address: {
      street: 'Victor Plains',
      suite: 'Suite 879',
      city: 'Wisokyburgh',
      zipcode: '90566-7771',
      geo: {
        lat: '-43.9509',
        lng: '-34.4618',
      },
    },
    phone: '010-692-6593 x09125',
    website: 'anastasia.net',
    company: {
      name: 'Deckow-Crist',
      catchPhrase: 'Proactive didactic contingency',
      bs: 'synergize scalable supply-chains',
    },
  },
]

export function provideUsersTest() {
  return usersTest
}

describe('Users  tests', () => {
  test('Users', () => {
    render(<Users />)
  })

  //describe('users list', async () => {
  /*
  const uTest=  usersTest

  jest.mock('../../../components/FetchComponent', () => ({
    __esModule: true,
    default: () => uTest,
  }));  
*/
  /*
  jest.spyOn(global, 'fetch')
  .mockImplementation(() => Promise.resolve({
    status: 200,
    json: () => Promise.resolve({
      value: usersTest
    })
  }));
  */
  /*
    global.fetch = jest.fn(() =>
    Promise.resolve({
      json: () => Promise.resolve(usersTest),
    })
  );
*/
  /*
  act(() => {
    jest.advanceTimersByTime(1000);
  })
*/
  // jest.spyOn(global, "fetch").mockImplementation(provideUsersTest());
  //const view = shallow(<Users />);
  /*
  const usersT = usersTest
  jest.mock('../../../components/FetchComponent', () => ({
    useFetch: () => {
        return usersT;
    },
 }));
 */
  /*
  it('renders headline', () => {
    render(<Users />)
  })
*/
  //expect(screen.getByText(`Loading`)).toBeInTheDocument()
  //expect(screen.getByText(`Leanne Graham`)).toBeInTheDocument()
  //console.log("data=" + data);
  // console.log("asFragment=" + asFragment);
  // const listUsers = await waitForElement(() => getByTestId('list-users'));
  //expect(getAllByRole('key').length).toBe(2)
  //screen.getByTestId("list-users");
})
