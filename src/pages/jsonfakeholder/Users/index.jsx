import { Table } from 'react-bootstrap'
import { Link, useParams } from 'react-router-dom'
import { useFetch } from '../../../components/FetchComponent'

function Users() {
  const { userId } = useParams()

  const { data, isLoading, error } = useFetch(import.meta.env.VITE_APP_URL_USERS)

  if (error) {
    return <span>Il y a un problème</span>
  }

  return (
    <div className="container mt-4">
      {isLoading ? (
        <div>Loading</div>
      ) : (
        <span>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>id</th>
                <th>Name</th>
                <th>UserName</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Website</th>
                <th>Posts</th>
                <th>Todos</th>
                <th>Albums</th>
              </tr>
            </thead>
            <tbody data-testid="list-users">
              {data
                ?.filter((item) => item.id == userId || userId == null)
                .map((item) => (
                  <tr key={item.id}>
                    <td>{item.id}</td>
                    <td>{item.name}</td>
                    <td>{item.username}</td>
                    <td>{item.email}</td>
                    <td>{item.phone}</td>
                    <td>{item.website}</td>
                    <td>
                      <Link to={`/jsonfakeholder/posts/${item.id}`}>posts</Link>
                    </td>
                    <td>
                      <Link to={`/jsonfakeholder/todos/${item.id}`}>todos</Link>
                    </td>
                    <td>
                      <Link to={`/jsonfakeholder/albums/${item.id}`}>albums</Link>
                    </td>
                  </tr>
                ))}
            </tbody>
          </Table>
        </span>
      )}
    </div>
  )
}

export default Users
