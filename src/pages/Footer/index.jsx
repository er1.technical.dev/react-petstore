import { Col, Container, Row } from 'react-bootstrap'
import { Link } from 'react-router-dom'

function Footer() {
  return (
    <Container fluid>
      <Row className="bg-dark text-white justify-content-md-center border border-2">
        <Col md="auto">
          <Link to={`/`}>Home</Link>
        </Col>
      </Row>
    </Container>
  )
}
export default Footer
