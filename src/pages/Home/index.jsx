import { Container } from 'react-bootstrap'

function Home() {
  return (
    <Container fluid>
      <span>Fill the home...😁.</span>
    </Container>
  )
}

export default Home
