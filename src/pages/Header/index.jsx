import { Container, Nav, NavDropdown, Navbar } from 'react-bootstrap'

function Header() {
  return (
    <Navbar variant="dark" bg="dark" expand="lg">
      <Container fluid>
        <Navbar.Brand href="/">Home</Navbar.Brand>
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav>
            <NavDropdown title="Jsonfakeholder" id="basic-nav-dropdown">
              <NavDropdown.Item href="/jsonfakeholder/users">Users</NavDropdown.Item>
              <NavDropdown.Item href="/jsonfakeholder/posts">Posts</NavDropdown.Item>
              <NavDropdown.Item href="/jsonfakeholder/todos">Todos</NavDropdown.Item>
              <NavDropdown.Item href="/jsonfakeholder/comments">Comments</NavDropdown.Item>
              <NavDropdown.Item href="/jsonfakeholder/albums">Albums</NavDropdown.Item>
            </NavDropdown>
            <NavDropdown title="miscellaneous" id="basic-nav-dropdown">
              <NavDropdown.Item href="/samples/useMemo">useMemo sample</NavDropdown.Item>
              <NavDropdown.Divider />
            </NavDropdown>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  )
}
export default Header
