import { useState } from 'react'
import { Button } from 'react-bootstrap'
import Form from 'react-bootstrap/Form'
import Col from 'react-bootstrap/esm/Col'
import { clearItemToMemoDatasMap, useMemoWithArray, useMemoWithMap } from '../../../components/usememo'

function UseMemoSample() {
  function onSubmit() {
    console.log('onSubmit')
    event.preventDefault()
  }

  const [inputData, setInputData] = useState(0)

  const resultWithMap = useMemoWithMap(inputData)
  const resultWithArray = useMemoWithArray(inputData)

  let resultM = resultWithMap.result
  let durationM = resultWithMap.duration
  let sizeM = resultWithMap.size

  let resultA = resultWithArray.result
  let durationA = resultWithArray.duration
  let sizeA = resultWithArray.size

  return (
    <span>
      <Form onSubmit={onSubmit}>
        <Form.Group as={Col} controlId="formGroupinputData">
          <Form.Label>inputData {Number.MAX_SAFE_INTEGER}</Form.Label>
          <Form.Control type="text" max={5} maxLength={15} placeholder="Enter inputData" value={inputData} onChange={(e) => setInputData(e.target.value)} />
        </Form.Group>
        <Button variant="primary" type="submit">
          Submit
        </Button>
        <Button variant="secondary" onClick={clearItemToMemoDatasMap}>
          Clear Map
        </Button>
      </Form>
      <div>
        useMemoWithMap: input = {inputData}: result = {resultM}. Duration: {durationM} ms: size = {sizeM}
      </div>
      <div>
        useMemoWithArray: input = {inputData}: result = {resultA}. Duration: {durationA} ms: size = {sizeA}
      </div>
    </span>
  )
}

export default UseMemoSample
