[![pipeline status](https://gitlab.com/er1.technical.dev/react-petstore/badges/evol_migration_vite/pipeline.svg)](https://gitlab.com/er1.technical.dev/react-petstore/-/commits/evol_migration_vite)
[![coverage report](https://gitlab.com/er1.technical.dev/react-petstore/badges/evol_migration_vite/coverage.svg)](https://gitlab.com/er1.technical.dev/react-petstore/-/commits/evol_migration_vite)

# React Application sample

# Table of contents

- [Table of contents](#table-of-contents)
- [1. Introduction](#1-introduction)

# 1. Introduction

Run with:

```bash
npm run start
```

Access to :

```bash
http://localhost:3000
```

Add typescript support:

```bash
npm install --save typescript @types/node @types/react @types/react-dom @types/jest
```

Use lint (see package.json):

```bash
npm run lint
```

Use test once:

```bash
npm run test:ci
```

Use code coverage:

```bash
npm run coverage
```
